function getMap() {
    $.ajax({
        url: `http://api.coffeein.md/api/companies/slug/${companySlug}`,
        type: 'Get',
        dataType: 'json',
        success: function (res) {
            let coordinates = `${res.coordinates}`
            let coordinate = coordinates.split(',');
            var map = L.map('map').setView([48.1, 28.15], 10);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            var greenIcon = L.icon({
                iconUrl: 'http://coffeein.md/assets/img/icons/map-marker.png',

                iconSize: [16, 16],
                popupAnchor: [0, -8]
            });
            L.marker([coordinate[1], coordinate[2]], {icon: greenIcon}).addTo(map)
                .bindPopup(coordinate[0]).openPopup();


        }
    });
}

function getMap2() {
    document.getElementById('map3').style.display = "none";
            var map = L.map('map2').setView([48.1, 28.15], 10);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
}


