var companySlug, slug, type, search, language, l;
if (JSON.parse(localStorage.getItem('lang_array'))!==null){
    changeLanguage = JSON.parse(localStorage.getItem('lang_array'));
}else {
    changeLanguage = 0;
}

slug = localStorage.getItem(`slugs`).toString();
let productData, html = ``, totalPrice = ``;
let serverApi = 'http://api.coffeein.md/api/';
window.onload = () => {
    setCookie('companySlug', slug, 30);
    companySlug = getCookie('companySlug');
    language = getCookie('lang');
    l = parseInt(getCookie('lang-item'));
    getNavbar();
    getLanguages();
    getFooter();
    getMap();
    getWholePrice();
    onloadPrice();
    search = getSearchParams('searchProducts');
    type = getCookie('TypeProducts');
    getProductData(type);
    /*
            const searchProducts = getSearchParams('searchProducts');
            console.log('searchProducts: ',searchProducts);
            document.getElementById('find-products').value = "daaaaaaa";*/
    getPopupSendCart();
    getProductCart();
    getFinishCommand();

}

function getNavbar() {

    $.ajax({
        url: `http://api.coffeein.md/api/companies/slug/${companySlug}`,
        type: 'Get',
        success: function (res) {
            let navbar = `<div class="nav-menu">
        <div class="nav-links">
        <div id="nav-links"></div>
        </div>
        <div class="nav-logo">
            <a href=""><img src="${serverApi + res.logo.path}"></a>
        </div>
        <div class="tell-number">
            <a href="tel:${res.phone}">${res.phone}</a>
        </div>
             </div> <div id="background-menu"><div class="background-menu">
        <div class="menu-content">
        ${changeLanguage.MENU}
        </div>
    </div>
</div>
<div class="come-back">
    <a href=".." ><button onclick="setCookie('TypeProducts', 'all', 30)"><img src="../assets/prev-arrow.png">   ${changeLanguage.BACK}</button></a>
</div>
`

            document.getElementById('navbar').innerHTML = navbar;

        }

    })


}

function getLanguages() {

    $.ajax({
        url: `http://api.coffeein.md/api/languages`,
        type: 'Get',
        dataType: 'json',
        success: function (res) {
            let languages = ``;
            for (let i = 0; i < res.docs.length; i++) {
                languages += `<a style="cursor: pointer; text-transform: uppercase" onclick="getTranslate('${res.docs[i].slug}',${i}); window.location.reload()" id="language-${res.docs[i].slug}">${res.docs[i].slug}</a>`
            }
            document.getElementById('nav-links').innerHTML = languages;
        }
    });

}


function getProductData(type) {
    let types;
    if (type !== null) {
        types = type;
    } else {
        types = "all";
        setSearchParams('productCategory', "all");
    }
    let searchProducts;
    if (search !== null) {
        searchProducts = search;
    } else {
        searchProducts = "";
    }
    let s = 0;
    $.ajax({
        url: `http://api.coffeein.md/api/products/sorted?page=1&searchTerm=${searchProducts}&productCategory=${types}&company=${companySlug}`,
        type: 'Get',
        dataType: 'json',
        success: function (res) {
            html = ``;

            productData = res;


            html += `


<div class="drinks-items">
<div class="types-drinks">

<button id="all-drinks" onclick="getTypeProducts('all')">${changeLanguage.ALL}</button>`
            for (let m = 0; m < productData.categories.length; m++) {
                html += `    <button id="warm-drinks" onclick="getTypeProducts('${productData.categories[m].slug}')">${productData.categories[m].name[l].content}</button>`
            }
            html += `</div>
<div class="drinks-input">
    <input type="text" placeholder="${changeLanguage.SEARCH}" id="find-products" class="find" onchange="setSearchParams('searchProducts', this.value)">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
         width="16" height="16"
         viewBox="0 0 50 50"
         style=" fill:#8C8C8C;"><path d="M 21 3 C 11.621094 3 4 10.621094 4 20 C 4 29.378906 11.621094 37 21 37 C 24.710938 37 28.140625 35.804688 30.9375 33.78125 L 44.09375 46.90625 L 46.90625 44.09375 L 33.90625 31.0625 C 36.460938 28.085938 38 24.222656 38 20 C 38 10.621094 30.378906 3 21 3 Z M 21 5 C 29.296875 5 36 11.703125 36 20 C 36 28.296875 29.296875 35 21 35 C 12.703125 35 6 28.296875 6 20 C 6 11.703125 12.703125 5 21 5 Z"></path></svg>
</div>
</div>
`
            for (let i = 0; i < productData.products.length; i++) {
                html += `<div class="cold-drinks-title">
            <h1 id="cold-drinks-title">${productData.products[i].category.name[l].content}</h1>
        </div>
        <div class="cold">
`

                for (let j = 0; j < productData.products[i].products.length; j++) {
                    html += `
            <div class="cold-drinks-menu">

        <div class="cold-drinks-product">
            <div class="cold-drinks-products">
                <div id="cold-drinks-product">
                    <img  src="${serverApi + productData.products[i].products[j].image.path}">
                    <h1  class="title">${productData.products[i].products[j].title[l].content}</h1>
                    <div class="cold-drinks-ingredients" onmouseover="getIngredientsColdStart(${s})" onmouseout="getIngredientsColdEnd(${s})" >
                    <p>${productData.products[i].products[j].mainDescription[l].content}</p>
                    <div id="description-cold-${s}" class="des">${productData.products[i].products[j].description[l].content}</div>
                    </div>
                </div>
                <div class="cold-drinks-buy">
                    <div class="cold-drinks-price">
                        <h1 id="price">${productData.products[i].products[j].price} <span> lei</span></h1>
                    </div>
                    <div class="cold-drinks-cart">
                        <button onclick="getPrice('${productData.products[i].products[j]._id}', ${s}, '${slug}')" class="${s}-clicked-${slug}" id="${slug}-clicked-${s}">${changeLanguage.TO_CART} <img src="../assets/products.png"></button>
                    </div>
                </div>
            </div>
        </div>
</div>



            `
                    s = s + 1;
                }
                html += `</div>`;

            }


            document.getElementById('body').innerHTML = html;


        }
    });
}

function getFooter() {
    $.ajax({
        url: `http://api.coffeein.md/api/companies/slug/${companySlug}`,
        type: 'Get',
        dataType: 'json',
        success: function (res) {

            let footer = ` <div id="footer-body">
        <div class="footer-contacts">
            <img src="${serverApi + res.logo.path}" class="footer-logo">
                <div id="footer-contacts">
                    <div class="footer-data">
                        <div class="phone-footer">
                            <a href="tel:${res.phone}"> <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                                          width="19" height="19"
                                                          viewBox="0 0 172 172"
                                                          style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-size="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#ffffff"><path d="M57.33333,17.18881c0.72787,0 0.72787,0 1.42774,0.72786c0.72786,0.72787 2.88347,2.85547 2.88347,4.3112c0,1.42773 0,5.01106 0.69987,7.86653c1.42774,3.58333 2.88347,12.9056 5.01107,17.91667c2.1556,5.01107 2.88346,7.89453 0.72786,10.75c-2.1556,3.58333 -15.76107,17.2168 -15.76107,17.2168c0,0 3.58333,7.16667 7.86654,11.44987c3.58333,5.01107 9.32226,12.17774 16.48893,17.91667c7.16667,6.4668 15.06119,12.17774 22.92774,15.76107c7.89453,-7.86654 14.33333,-14.33333 16.48893,-15.76107c2.1556,-1.42774 2.1556,-2.1556 5.73893,-0.69987c3.58333,1.42773 8.5944,3.58333 14.33333,4.2832c5.01107,0.72786 10.75,1.42774 14.33333,2.1556c3.58333,0.72787 3.58333,0.72787 4.3112,2.1556c1.42773,1.42774 2.1276,3.58333 2.1276,4.28321c0,0.72786 0,2.88346 0,10.05012c0,7.16667 0,17.18881 0,19.3444c0,2.1556 0,2.1556 -0.69988,3.58333c-0.72786,1.42774 -1.42773,5.01107 -1.42773,5.01107c0,0.72786 -3.58333,2.1556 -5.73894,2.1556c-2.1556,0 -10.05012,0 -15.06119,-0.72786c-5.01107,-0.69988 -18.61653,-3.58333 -23.6556,-5.71094c-5.01107,-1.45573 -21.5,-7.16667 -41.54427,-22.22787c-18.64453,-14.33333 -32.25,-33.67774 -40.14453,-49.4388c-7.89453,-15.78906 -12.17774,-32.97787 -12.9056,-41.57227c-1.42774,-8.5944 -1.42774,-12.17773 -1.42774,-14.33333c0.72787,-2.1556 2.85547,-4.31119 4.3112,-5.01106c1.42773,-0.72787 3.58333,-1.45573 5.71094,-1.45573c2.1556,0 7.89453,0 15.78906,0c7.86653,0 17.1888,0 17.1888,0z"></path></g></g></svg>
                            </a>
                            <a href="tel:${res.phone}">${res.phone}</a>
                        </div>
                        <div class="email-footer">
                            <a href="mailto:${res.mail}" ><svg width="19" height="19" viewBox="0 0 18 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M15.6667 0.833008H2.33335C1.67837 0.833008 1.10848 1.21592 0.836479 1.76894C0.927313 1.79002 1.01571 1.8285 1.09678 1.88525L9.00002 7.41752L16.9033 1.88525C16.9843 1.8285 17.0727 1.79002 17.1636 1.76894C16.8916 1.21592 16.3217 0.833008 15.6667 0.833008ZM17.3334 3.41517L9.71685 8.74672C9.28645 9.048 8.71359 9.048 8.28319 8.74672L0.666687 3.41517V12.4997C0.666687 13.4163 1.41669 14.1663 2.33335 14.1663H15.6667C16.5834 14.1663 17.3334 13.4163 17.3334 12.4997V3.41517Z" fill="#EAE9E9"/>
                            </svg></a>
                            <a href="mailto:${res.mail}">${res.mail}</a>
                        </div>
                    </div>
                    <img src="../assets/bar.png" class="footer-bar">
                        <div class="media">
                            <a href="${res.instagram}" ><div class="instagram">
                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     width="21" height="22"
                                     viewBox="0 0 172 172"
                                     style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-size="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="#3a3a3a"></path><g fill="#ffffff"><path d="M55.04,10.32c-24.6648,0 -44.72,20.0552 -44.72,44.72v61.92c0,24.6648 20.0552,44.72 44.72,44.72h61.92c24.6648,0 44.72,-20.0552 44.72,-44.72v-61.92c0,-24.6648 -20.0552,-44.72 -44.72,-44.72zM127.28,37.84c3.784,0 6.88,3.096 6.88,6.88c0,3.784 -3.096,6.88 -6.88,6.88c-3.784,0 -6.88,-3.096 -6.88,-6.88c0,-3.784 3.096,-6.88 6.88,-6.88zM86,48.16c20.8808,0 37.84,16.9592 37.84,37.84c0,20.8808 -16.9592,37.84 -37.84,37.84c-20.8808,0 -37.84,-16.9592 -37.84,-37.84c0,-20.8808 16.9592,-37.84 37.84,-37.84zM86,55.04c-17.0624,0 -30.96,13.8976 -30.96,30.96c0,17.0624 13.8976,30.96 30.96,30.96c17.0624,0 30.96,-13.8976 30.96,-30.96c0,-17.0624 -13.8976,-30.96 -30.96,-30.96z"></path></g></g></svg>
                            </div></a>
                            <a href="${res.youtube}" ><div class="youtube">
                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     width="24" height="25"
                                     viewBox="0 0 172 172"
                                     style=" fill:#000000;"><defs><linearGradient x1="35.174" y1="35.37108" x2="147.79817" y2="147.99525" gradientUnits="userSpaceOnUse" id="color-1_9a46bTk3awwI_gr1"><stop offset="0" stop-color="#ffffff"></stop><stop offset="0.443" stop-color="#ffffff"></stop><stop offset="1" stop-color="#ffffff"></stop></linearGradient></defs><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-size="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="#3a3a3a"></path><g><path d="M161.293,123.84c-1.57308,8.02667 -8.256,14.14342 -16.512,15.29008c-12.97525,1.90992 -34.60067,4.20325 -58.97808,4.20325c-23.98325,0 -45.60867,-2.29333 -58.97808,-4.20325c-8.256,-1.14667 -14.9425,-7.26342 -16.512,-15.29008c-1.57308,-8.78992 -3.14617,-21.78667 -3.14617,-37.84c0,-16.05333 1.57308,-29.05008 3.14617,-37.84c1.57308,-8.02667 8.256,-14.14342 16.512,-15.29008c12.97525,-1.90992 34.60067,-4.20325 58.97808,-4.20325c24.37742,0 45.60867,2.29333 58.97808,4.20325c8.256,1.14667 14.9425,7.26342 16.512,15.29008c1.57308,8.78992 3.54033,21.78667 3.54033,37.84c-0.39417,16.05333 -1.96725,29.05008 -3.54033,37.84z" fill="url(#color-1_9a46bTk3awwI_gr1)"></path><path d="M115.928,80.41l-40.979,-27.31933c-2.06758,-1.37958 -4.7085,-1.50858 -6.89792,-0.33325c-2.18942,1.17175 -3.55108,3.44 -3.55108,5.92325v54.63867c0,2.48325 1.36167,4.75508 3.55108,5.92683c0.99617,0.53392 2.08192,0.7955 3.16767,0.7955c1.30433,0 2.6015,-0.37983 3.72667,-1.12875l40.979,-27.31933c1.87408,-1.25058 2.99208,-3.33967 2.99208,-5.59c0.00358,-2.25392 -1.118,-4.343 -2.9885,-5.59358z" fill="#ffffff" opacity="0.05"></path><path d="M74.10692,54.59925l38.66417,25.7785c2.46892,1.77375 4.13158,3.36117 4.13158,5.42158c0,2.06042 -0.80267,3.49733 -2.56208,4.78017c-1.32942,0.9675 -39.57792,26.38767 -39.57792,26.38767c-3.22858,2.16433 -8.471,1.70567 -8.471,-5.37142v-51.59642c0,-7.18458 5.676,-6.82625 7.81525,-5.40008z" fill="#ffffff" opacity="0.07"></path><path d="M68.08333,113.11867v-54.23375c0,-2.66242 2.967,-4.25342 5.18508,-2.7735l40.678,27.11867c1.98158,1.31867 1.98158,4.22833 0,5.55058l-40.678,27.11867c-2.21808,1.47275 -5.18508,-0.11467 -5.18508,-2.78067z" fill="#3a3a3a"></path></g></g></svg>
                            </div></a>
                            <a href="${res.facebook}" ><div class="facebook">
                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                     width="24" height="25"
                                     viewBox="0 0 172 172"
                                     style=" fill:#000000;"><defs><linearGradient x1="35.80825" y1="35.80825" x2="145.53708" y2="145.53708" gradientUnits="userSpaceOnUse" id="color-1_uLWV5A9vXIPu_gr1"><stop offset="0" stop-color="#3a3a3a"></stop><stop offset="1" stop-color="#3a3a3a"></stop></linearGradient></defs><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-size="none"  style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="#3a3a3a"></path><g><path d="M86,14.33333c-39.5815,0 -71.66667,32.08517 -71.66667,71.66667c0,39.5815 32.08517,71.66667 71.66667,71.66667c39.5815,0 71.66667,-32.08517 71.66667,-71.66667c0,-39.5815 -32.08517,-71.66667 -71.66667,-71.66667z" fill="url(#color-1_uLWV5A9vXIPu_gr1)"></path><path d="M95.70008,104.99525h18.54733l2.91325,-18.84117h-21.46058v-10.2985c0,-7.826 2.5585,-14.76692 9.87925,-14.76692h11.76408v-16.44033c-2.06758,-0.2795 -6.43925,-0.88867 -14.69883,-0.88867c-17.25017,0 -27.36233,9.10883 -27.36233,29.8635v12.5345h-17.73392v18.84117h17.73033v51.78633c3.51167,0.52317 7.06992,0.8815 10.72133,0.8815c3.30025,0 6.52167,-0.301 9.70008,-0.731z" fill="#ffffff"></path></g></g></svg>
                            </div></a>
                        </div>
                </div>
        </div>
<div id="map"></div>
    </div>
    <h1 class="footer-message">Developed with love by <span>LOOPIT</span></h1>`;


            document.getElementById('footer').innerHTML = footer;


        }

    })
}

function getWholePrice() {

    document.getElementById('total-price').innerHTML = `
    <div id="whole-price-products">
    <a href="cart.html">
        <div class="whole-price-products" >
            <div class="whole-price">
                <h1>${changeLanguage.FINISH_ORDER}</h1>
                <p>${changeLanguage.TOTAL} <b id="whole-price"></b><span>LEI</span></p>
            </div>
            <div class="types-products">
                <div class="title" ><h1 id="times"></h1></div>
            </div>
            </div>
        </div>`


}

function getIngredientsColdStart(b) {
    document.getElementById('description-cold-' + b).style.display = "block";
}

function getIngredientsColdEnd(b) {
    document.getElementById('description-cold-' + b).style.display = "none";
}






